'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">ngx-gallery</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="changelog.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CHANGELOG
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/NgxGalleryModule.html" data-type="entity-link">NgxGalleryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NgxGalleryModule-b735c9a35870f5eeb08e3876c9a4ce0c"' : 'data-target="#xs-components-links-module-NgxGalleryModule-b735c9a35870f5eeb08e3876c9a4ce0c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NgxGalleryModule-b735c9a35870f5eeb08e3876c9a4ce0c"' :
                                            'id="xs-components-links-module-NgxGalleryModule-b735c9a35870f5eeb08e3876c9a4ce0c"' }>
                                            <li class="link">
                                                <a href="components/NgxGalleryActionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryActionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxGalleryArrowsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryArrowsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxGalleryBulletsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryBulletsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxGalleryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxGalleryImageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryImageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxGalleryPreviewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryPreviewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxGalleryThumbnailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NgxGalleryThumbnailsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/CustomHammerConfig.html" data-type="entity-link">CustomHammerConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryAction.html" data-type="entity-link">NgxGalleryAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryAnimation.html" data-type="entity-link">NgxGalleryAnimation</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryImage.html" data-type="entity-link">NgxGalleryImage</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryImageSize.html" data-type="entity-link">NgxGalleryImageSize</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryLayout.html" data-type="entity-link">NgxGalleryLayout</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryOptions.html" data-type="entity-link">NgxGalleryOptions</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryOrder.html" data-type="entity-link">NgxGalleryOrder</a>
                            </li>
                            <li class="link">
                                <a href="classes/NgxGalleryOrderedImage.html" data-type="entity-link">NgxGalleryOrderedImage</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/NgxGalleryHelperService.html" data-type="entity-link">NgxGalleryHelperService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/INgxGalleryAction.html" data-type="entity-link">INgxGalleryAction</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INgxGalleryImage.html" data-type="entity-link">INgxGalleryImage</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INgxGalleryOptions.html" data-type="entity-link">INgxGalleryOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INgxGalleryOrderedImage.html" data-type="entity-link">INgxGalleryOrderedImage</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});